package com.example.btsqlite_1312536_1312561;

public class LopInfo {
	private Integer maLop;
	private String tenLop;
	
	public LopInfo(Integer malop, String tenlop) {
		this.maLop = malop;
		this.tenLop = tenlop;
	}
	
	public String getTenLop() {
		return tenLop;
	}
	public void setTenLop(String tenLop) {
		this.tenLop = tenLop;
	}
	public int getMaLop() {
		return maLop;
	}
	public void setMaLop(int maLop) {
		this.maLop = maLop;
	}
	
	@Override
	public String toString() {
		return this.tenLop;
	}
	
}
