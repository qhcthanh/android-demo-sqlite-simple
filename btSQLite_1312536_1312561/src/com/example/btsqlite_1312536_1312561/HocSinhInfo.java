package com.example.btsqlite_1312536_1312561;

public class HocSinhInfo {
	private Integer maLop;
	private String tenHS;
	private Integer maHS;
	
	public HocSinhInfo(Integer malop, String ten, Integer mahs) {
		this.maLop = malop;
		this.tenHS = ten;
		this.maHS = mahs;
	}
	
	public String getTenHS() {
		return tenHS;
	}
	public void setTenHS(String tenHS) {
		this.tenHS = tenHS;
	}
	public int getMaLop() {
		return maLop;
	}
	public void setMaLop(int maLop) {
		this.maLop = maLop;
	}
	public int getMaHS() {
		return maHS;
	}
	public void setMaHS(int maHS) {
		this.maHS = maHS;
	}
	
	@Override
	public String toString() {
		return this.maHS + " - " + this.tenHS ;
	}
}
