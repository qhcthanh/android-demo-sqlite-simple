package com.example.btsqlite_1312536_1312561;

import java.util.ArrayList;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;

public class MainActivity extends Activity {
	Button taiLopBtn;
	Spinner dsLopSpinner;
	ListView dsHocSinhListView;
	SQLiteDatabase db;
	ArrayAdapter<LopInfo> dataAdapter;
	ArrayAdapter<HocSinhInfo> hsDataAdapter;
	ArrayList<LopInfo> ds_Lop = new ArrayList<LopInfo>();
	ArrayList<HocSinhInfo> ds_Hs = new ArrayList<HocSinhInfo>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		taiLopBtn = (Button) findViewById(R.id.button1);
		dsLopSpinner = (Spinner) findViewById(R.id.lopSpinner);
		dsHocSinhListView = (ListView) findViewById(R.id.listView1);
		
		db = this.openOrCreateDatabase("QLHS", MODE_PRIVATE, null);
		
		ArrayList<LopInfo> dsLop = new ArrayList<LopInfo>();
		dsLop.add(new LopInfo(0,"CTT1"));
		dsLop.add(new LopInfo(1,"CTT2"));
		dsLop.add(new LopInfo(2,"CTT3"));
		ArrayList<HocSinhInfo> dsHocSinh = new ArrayList<HocSinhInfo>();
		dsHocSinh.add(new HocSinhInfo(0,"Quach Ha Chan Thanh",1312536) );
		dsHocSinh.add(new HocSinhInfo(1,"Nguyen Duc Thinh",1312561) );
		dsHocSinh.add(new HocSinhInfo(2,"Ho Doan Ba Thien",1312549) );
		dsHocSinh.add(new HocSinhInfo(0,"Tran Truong Trieu Thien",1312557) );
		dsHocSinh.add(new HocSinhInfo(1,"Nguyen Xuan Thai",1312525) );
		dsHocSinh.add(new HocSinhInfo(2,"Hoang Khanh Thien",1312548) );
		dsHocSinh.add(new HocSinhInfo(2,"Vo Minh Quoc",1312467) );
		
		try { 
			db.execSQL("CREATE TABLE IF NOT EXISTS LOP ("
			+ " malop integer PRIMARY KEY autoincrement, "
			+ " tenlop text ); ");
	db.execSQL("CREATE TABLE IF NOT EXISTS HOCSINH ("
			+ " mahs integer PRIMARY KEY, "
			+ " tenhs text, "
			+ " malop integer ); ");
			for(LopInfo lop : dsLop) {
				try {
				String query = String.format("insert into LOP (malop,tenlop) values (%d,'%s');",lop.getMaLop(),lop.getTenLop() );
				db.execSQL(query);
				} catch (Exception e) {
					
				}
			}
			
			for(HocSinhInfo hs : dsHocSinh) {
				try {
				String query = String.format("insert into HOCSINH (malop,mahs,tenhs) values (%d,%d,'%s');",hs.getMaLop(),hs.getMaHS(),hs.getTenHS() );
				db.execSQL(query);
				} catch(Exception e) {
					
				}
			}
		} catch (Exception e) {
			// Kệ 
		}

		dataAdapter = new ArrayAdapter<LopInfo>(this, android.R.layout.simple_spinner_item, ds_Lop);
		hsDataAdapter = new ArrayAdapter<HocSinhInfo>(this,android.R.layout.simple_list_item_1,ds_Hs);
		dsLopSpinner.setAdapter(dataAdapter);
		taiLopBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Cursor c1 = db.rawQuery("select *from lop", null);
				c1.moveToPosition(-1);
				ds_Lop.removeAll(ds_Lop);
				while (c1.moveToNext()) {
					int malop = c1.getInt(0);
					String tenlop = c1.getString(1);
					ds_Lop.add(new LopInfo(malop,tenlop));
				}
				dsLopSpinner.setAdapter(dataAdapter);
			}
		});
		
		dsLopSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				LopInfo lop = ds_Lop.get(position);
				Cursor c1 = db.rawQuery("select *from hocsinh where malop = " + lop.getMaLop(), null);
				c1.moveToPosition(-1);
				ds_Hs.removeAll(ds_Hs);
				while (c1.moveToNext()) {
					int mahs = c1.getInt(0);
					String tenhs = c1.getString(1);
					ds_Hs.add(new HocSinhInfo(lop.getMaLop(),tenhs,mahs));
				}
				dsHocSinhListView.setAdapter(hsDataAdapter);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		
	}
	
	
	
	
}
